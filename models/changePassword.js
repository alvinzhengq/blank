const jwt = require('jsonwebtoken');
const updatePassword = require('./mongoDB').updatePassword;

function changePassword(postData, configs, cb){
    jwt.verify(postData.authToken, configs.authSecret, (err, token) => {
        if(err) {
            cb("INVALID");
        }else{
            var username = token.data.user;
            updatePassword("username", username, postData.password, postData.salt)
            cb("GOOD");
        }
    })
}

module.exports.changePassword = changePassword;