const redis = require('redis')
const crypto = require('crypto')
const nodemailer = require('nodemailer')
const mongoDB = require('./mongoDB');

const REDIS_PORT = 6379;
const client = redis.createClient(REDIS_PORT);

function handleRegistration(userData, configs, callback){
    mongoDB.checkUserName(userData, function(result){
        if(result == "OK"){
            mongoDB.checkEmail(userData, function(result2){
                if(result2 == "OK"){
                    var verificationCode = crypto.randomBytes(8).toString('hex');
                    userData.code = verificationCode;

                    client.setex(userData.username, 3600, JSON.stringify(userData));
                    mongoDB.reserveUserInfo(userData);
                    mongoDB.unreserveUserInfo(userData);

                    var smtpTransport = nodemailer.createTransport({
                        service: "gmail",
                        auth: {
                            user: configs.gmail,
                            pass: configs.gmail_pass
                        }
                    })

                    var message = {
                        from: "Blank",
                        to: userData.email,
                        subject: "Verify your Blank Chat Client Account",
                        html: "<h3>Your verification string is: " + verificationCode + "</h3>"
                    }

                    smtpTransport.sendMail(message, function(err, res){
                        if(err) console.log(err);
                    })

                    callback("OK")
                }else{
                    callback("BAD_EMAIL")
                }
            })
        }else{
            callback("BAD_USERNAME")
        }
    })
}

module.exports.handleRegistration = handleRegistration;