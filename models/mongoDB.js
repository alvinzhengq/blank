var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/UserDB"

function newUser(userData, machineID){
    MongoClient.connect(url, {poolSize: 3}, (err, client) => {
        var db = client.db("UserDB")

        db.collection('primaryUserCollection').insertOne({
            username: userData.username,
            password: userData.password,
            salt: userData.salt,
            email: userData.email,
            refreshToken: "",
            current_key: "",
            primaryDeviceID: machineID,
            chatList: []
        })
        .then(()=>{
            db.collection('reservedUsername').insertOne({username: userData.username}).then(()=>{
                db.collection('reservedEmails').insertOne({email: userData.email}, function(err, result){
                    if(err) throw err;
                    client.close();
                })
            })
        })
    })
}

function verifyEmailExists(email, cb){
    MongoClient.connect(url, {poolSize: 1}, function(err, client){
        var db = client.db("UserDB");

        db.collection('primaryUserCollection').findOne({email: email}, function(err, result){
            if(err){
                cb("ERROR");
                throw err;
            }else{
                if(result != null){
                    cb(true)
                }else{
                    cb(false)
                }
            }
            client.close()
        })
    })
}

function setRefreshToken(username, token){
    MongoClient.connect(url, {poolSize: 1}, function(err, client){
        var db = client.db("UserDB");

        db.collection('primaryUserCollection').updateOne({username: username}, {$set: {refreshToken: token}}, function(err, result){
            if(err) throw err;
            client.close()
        })
    })
}

function updateKey(username, key){
    MongoClient.connect(url, {poolSize: 1}, function(err, client){
        var db = client.db("UserDB");

        db.collection('primaryUserCollection').updateOne({username: username}, {$set: {current_key: key}}, function(err, result){
            if(err) throw err;
            client.close()
        })
    })
}

function updatePassword(query_type, query, password, salt){
    if(query_type === "email"){
        MongoClient.connect(url, {poolSize: 1}, function(err, client){
            var db = client.db("UserDB");

            db.collection('primaryUserCollection').updateOne({email: query}, {$set: {password: password, salt: salt}}, function(err, result){
                if(err) throw err;
                client.close()
            })
        })
    }else if(query_type === "username"){
        MongoClient.connect(url, {poolSize: 1}, function(err, client){
            var db = client.db("UserDB");

            db.collection('primaryUserCollection').updateOne({username: query}, {$set: {password: password, salt: salt}}, function(err, result){
                if(err) throw err;
                client.close()
            })
        })
    }
}

function updateEmail(email, username){
    MongoClient.connect(url, {poolSize: 1}, function(err, client){
        var db = client.db("UserDB");

        db.collection('primaryUserCollection').updateOne({username: username}, {$set: {email: email}}, function(err, result){
            if(err) throw err;

            client.close()
        })
    })
}

function getUser(username, cb){
    MongoClient.connect(url, {poolSize: 1}, function(err, client){
        var db = client.db("UserDB");

        db.collection('primaryUserCollection').findOne({username: username}, function(err, result){
            if(err){
                cb("ERROR");
                throw err;
            }
            cb(result);
            client.close()
        })
    })
}

function reserveUserInfo(userData){
    MongoClient.connect(url, {poolSize: 2}, function(err, client) {
        var db = client.db("UserDB")

        db.collection('reservedUsername').insertOne({username: userData.username}).then(()=>{
            db.collection('reservedEmails').insertOne({email: userData.email}, function(err, result){
                if(err) throw err;
            })
            client.close()
        })
    })
}

function unreserveUserInfo(userData){
    setTimeout(()=>{
        MongoClient.connect(url, {poolSize: 2}, function(err, client) {
            var db = client.db("UserDB")

            db.collection('reservedUsername').deleteOne({username: userData.username}).then(()=>{
                db.collection('reservedEmails').deleteOne({email: userData.email}, function(err, result){
                    if(err) throw err;
                    client.close();
                })
            })
        })
    }, 3600000);
}

function checkUserName(userData, cb){
    MongoClient.connect(url, {poolSize: 1}, function(err, client) {
        var db = client.db("UserDB")

        db.collection('reservedUsername').find({username: userData.username}).toArray(function(err1, result){
            if(result.length == 0){
                cb("OK")
            }else{
                cb("U_RESERVED")
            }

            client.close()
        })
    })
}

function checkEmail(userData, cb){
    MongoClient.connect(url, {poolSize: 1}, function(err, client) {
        var db = client.db("UserDB")

        db.collection('reservedEmails').find({email: userData.email}).toArray(function(err1, result){
            if(result.length == 0){
                cb("OK")
            }else{
                cb("E_RESERVED")
            }
            client.close()
        })
    })
}

function deleteAccount(username){
    MongoClient.connect(url, {poolSize: 4}, function(err, client) {
        var db = client.db("UserDB")

        db.collection('primaryUserCollection').findOne({username: username}, function(err, result){
            if(err) throw err;

            db.collection('primaryUserCollection').deleteOne({username: username})
            .then(()=>{
                db.collection('reservedEmails').deleteMany({email: result.email})
                .then(()=>{
                    db.collection('reservedUsername').deleteMany({username: username}).then(()=>{
                        client.close()
                    })
                })
            })
        })

    })
}

function retrieveChatList(username, cb){
    MongoClient.connect(url, {poolSize: 1}, function(err, client) {
        var db = client.db("UserDB")

        db.collection('primaryUserCollection').findOne({username: username}, function(err, result){
            if(err) throw err;

            cb(result.chatList)
        })

    })
}

function retrieveChatData(chatID, cb){
    MongoClient.connect(url, {poolSize: 1}, function(err, client) {
        var db = client.db("UserDB")

        db.collection('chatData').findOne({chatID: chatID}, function(err, result){
            if(err) throw err;

            cb(result)
        })

    })
}

function getUserPubKey(username, cb){
    MongoClient.connect(url, {poolSize: 1}, function(err, client) {
        var db = client.db("UserDB")

        db.collection('primaryUserCollection').findOne({username: username}, function(err, result){
            if(err) throw err;

            if(result == null){
                cb(null);
            }else{
                cb(result.current_key)
            }
        })

    })
}

module.exports = {
    newUser,
    checkUserName,
    checkEmail,
    reserveUserInfo,
    unreserveUserInfo,
    getUser,
    setRefreshToken,
    updateKey,
    updatePassword,
    updateEmail,
    verifyEmailExists,
    deleteAccount,
    retrieveChatList,
    retrieveChatData,
    getUserPubKey
};