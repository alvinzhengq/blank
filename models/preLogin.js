const redis = require('redis')
const crypto = require("crypto");
const NodeRSA = require('node-rsa')

const REDIS_PORT = 6379;
let client = redis.createClient(REDIS_PORT);

function preLogin(cb){
    var keyPair = new NodeRSA({b: 512}).generateKeyPair();
    var pubKey = keyPair.exportKey('pkcs1-public-pem');
    var privKey = keyPair.exportKey('pkcs1-private-pem');
    var id = crypto.randomBytes(16).toString("hex");

    client.setex(id, 60, privKey);

    cb(JSON.stringify({id: id, pubKey: pubKey}));
}

module.exports.preLogin = preLogin;