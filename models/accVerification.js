const redis = require('redis')
const newUser = require('./mongoDB').newUser;

const REDIS_PORT = 6379;
let client = redis.createClient(REDIS_PORT);

function verifyAccount(postData, callback){
    var code = postData.code;
    var username = postData.username;
    var machineID = postData.machineID;

    client.get(username, (err, reply) => {
        if(err){
            callback(err);
        }else{
            if(reply != null){
                var userData = JSON.parse(reply);

                if(code == userData.code){
                    delete userData.code;
                    client.del(userData.username, function(err, reply){})

                    newUser(userData, machineID);
                    callback("OK");
                    client.quit();
                }else{
                    callback("WRONG");
                    client.quit();
                }
            }else{
                callback("EXPIRE")
                client.quit();
            }
        }
    });
}

module.exports.verifyAccount = verifyAccount;