const redis = require('redis')
const crypto = require('crypto')
const nodemailer = require('nodemailer');
const { verifyEmailExists } = require('./mongoDB');

const REDIS_PORT = 6379;
let client = redis.createClient(REDIS_PORT);

function getResetCode(postData, configs, cb){
    verifyEmailExists(postData.email, (response) => {
        if(response == true){
            var verificationCode = crypto.randomBytes(8).toString('hex');
            client.setex(verificationCode, 120, postData.email, ()=>{
                client.quit();
            });

            var smtpTransport = nodemailer.createTransport({
                service: "gmail",
                auth: {
                    user: configs.gmail,
                    pass: configs.gmail_pass
                }
            })

            var message = {
                from: "Blank",
                to: postData.email,
                subject: "Password Reset Code for your Blank Chat Client Account",
                html: "<h3>Your code is: " + verificationCode + "</h3>"
            }

            smtpTransport.sendMail(message, function(err, res){
                if(err){
                    console.log(err);
                    cb("ERROR")
                }else{
                    cb("GOOD")
                }
            })
        }else{
            cb("NO_EXIST")
        }
    })
}

module.exports.getResetCode = getResetCode;