const redis = require('redis')
const updatePassword = require('./mongoDB').updatePassword;

const REDIS_PORT = 6379;
let client = redis.createClient(REDIS_PORT);

function resetPassword(postData, cb){
    client.get(postData.code, (err, response) => {
        if(response != null){
            client.del(postData.code)
            updatePassword("email", response, postData.password, postData.salt)
            cb("GOOD")
            client.quit();
        }else{
            cb("INVALID")
            client.quit();
        }
    })
}

module.exports.resetPassword = resetPassword;