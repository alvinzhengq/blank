const setRefreshToken = require('./mongoDB').setRefreshToken;
const jwt = require('jsonwebtoken')

function logout(postData, configs, cb){
    jwt.verify(postData.authToken, configs.authSecret, (err, token) => {
        if(err) {
            cb("INVALID");
        }else{
            var username = token.data.user;
            setRefreshToken(username, "");
            cb("GOOD");
        }
    })
}

module.exports.logout = logout;