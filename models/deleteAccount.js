const jwt = require('jsonwebtoken');
const deleteAccountDB = require('./mongoDB').deleteAccount;

function deleteAccount(postData, configs, cb){
    jwt.verify(postData.authToken, configs.authSecret, (err, token) => {
        if(err) {
            cb("INVALID");
        }else{
            var username = token.data.user;
            deleteAccountDB(username);
            cb("GOOD");
        }
    })
}

module.exports.deleteAccount = deleteAccount;