const getUser = require('./mongoDB').getUser;
const jwt = require('jsonwebtoken')

function getToken(postData, configs, cb){
    try{
        jwt.verify(postData.refreshToken, configs.refreshSecret, ()=>{
            getUser(postData.username, function(res){
                if(res != null && res.refreshToken == postData.refreshToken){
                    if(postData.machineID == res.primaryDeviceID){
                        jwt.sign({
                            data: { user: postData.username, type: "auth" }
                        }, configs.authSecret, { algorithm: "HS512", expiresIn: 60}, (err, token)=>{
                            cb(token)
                        })
                    }else{
                        cb("SECONDARY")
                    }
                }else{
                    cb("INVALID")
                }
            })
        })
    }catch(err){
        cb("INVALID")
    }
}

module.exports.getToken = getToken