const jwt = require('jsonwebtoken');
const updateEmail = require('./mongoDB').updateEmail;

function changeEmail(postData, configs, cb){
    jwt.verify(postData.authToken, configs.authSecret, (err, token) => {
        if(err) {
            cb("INVALID");
        }else{
            var username = token.data.user;
            updateEmail(postData.email, username)
            cb("GOOD");
        }
    })
}

module.exports.changeEmail = changeEmail;