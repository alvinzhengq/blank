const retrieveChatList = require('./mongoDB').retrieveChatList;
const retrieveChatData = require('./mongoDB').retrieveChatData;
const getUserPubKey = require("./mongoDB").getUserPubKey;
const redis = require('redis')

const REDIS_PORT = 6379;
let client = redis.createClient(REDIS_PORT);

module.exports.handleChatData = function handleChatData(username, cb){
    retrieveChatList(username, (chatList)=> {
        var chatDataList = []

        if(chatList.length == 0){
            cb([]);
        }

        for(i in chatList){
            retrieveChatData(chatList[i], (response) => {
                client.setnx(chatList[i], JSON.stringify(response.participantList));

                if(response.chatType == "DIRECT_MESSAGE"){
                    getUserPubKey(response.participantList.filter(x => [username].indexOf(x) === -1)[0], (pubKey) => {
                        response.pubKey = pubKey
                        chatDataList.push(response)
                        if(i == chatList.length-1){
                            cb(chatDataList);
                        }
                    })
                }else{
                    chatDataList.push(response)
                    if(i == chatList.length-1){
                        cb(chatDataList);
                    }
                }
            })
        }

    })
}