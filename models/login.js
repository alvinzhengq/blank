const jwt = require('jsonwebtoken')
const sha512 = require('js-sha512').sha512;
const getUser = require('./mongoDB').getUser;
const setRefreshToken = require('./mongoDB').setRefreshToken;

function login(postData, configs, cb){
    var username = postData.username;
    var password = postData.password;

    getUser(username, function(response){
        if(response != null){
            var hashedPass = keyStretch(password + response.salt);
            if(hashedPass == response.password){
                jwt.sign({
                    data: { user: username, type: "refresh" }
                }, configs.refreshSecret, { algorithm: "HS512"}, (err, token) => {
                    setRefreshToken(username, token)
                    cb(JSON.stringify({status: "GOOD", jwt: token}))
                })

            }else{
                cb(JSON.stringify({status: "INVALID"}));
            }
        }else{
            cb(JSON.stringify({status: "INVALID"}))
        }
    })
}

function keyStretch(password){
    for(let i = 0; i < 1000; i++){
        password = sha512(password);
    }

    return password;
}

module.exports.login = login;