const updatePubKey = require('./mongoDB').updateKey;

function updateKey(username, key){
    updatePubKey(username, key);
}

module.exports.updateKey = updateKey;