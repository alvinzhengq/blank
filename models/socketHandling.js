var socketioJwt = require('socketio-jwt');
var updatePubKey = require('./updateKey').updateKey;
var handleChatData = require('./handleChatData').handleChatData;
var getOfflineData = require('./getOfflineData').getOfflineData;

module.exports.socketHandling = function socketHandling(io, configs){
    io.on('connection', socketioJwt.authorize({
        secret: configs.authSecret,
        timeout: 30000
    }))

    io.on('authenticated', (socket) => {
        console.log(`hello! ${socket.decoded_token.data.user}`);

        socket.readyForMessages = false;
        socket.tempDataQ = []

        socket.on('getChatDataList', ()=>{
            handleChatData(socket.decoded_token.data.user, (chatDataList) => {
                socket.emit('chatDataList', chatDataList);

                for(i in chatDataList){
                    socket.join(chatDataList[i].chatID);
                }
            })
        })

        socket.on('getOfflineData', () => {
            getOfflineData(socket.decoded_token.data.user, (offlineDataList) => {
                socket.emit('offlineData', offlineDataList);
            })
        })

        socket.on('updateKey', (data) => {
            updatePubKey(socket.decoded_token.data.user, data);
        })
    });
}
