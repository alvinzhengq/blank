var express = require('express')
var http = require('http');
var redisAdapter = require('socket.io-redis');
var helmet = require('helmet')
var Ddos = require('ddos')
var NodeRSA = require('node-rsa');

var registration = require('./models/registration').handleRegistration;
var verification = require('./models/accVerification').verifyAccount;
var login = require('./models/login').login;
var getToken = require('./models/getToken').getToken;
var logout = require('./models/logout').logout;
var getResetCode = require('./models/getResetCode').getResetCode;
var resetPassword = require('./models/resetPassword').resetPassword;
var changePassword = require('./models/changePassword').changePassword;
var changeEmail = require('./models/changeEmail').changeEmail;
var deleteAccount = require('./models/deleteAccount').deleteAccount;
var socketHandling = require('./models/socketHandling').socketHandling;

var app = express()
var server = http.createServer(app);
var io = require('socket.io')(server);
io.adapter(redisAdapter({host: 'localhost', port: 6379}))

const configs = JSON.parse(require('fs').readFileSync('./config.txt'))
const apiSecret = configs.apiSecret;

var key = new NodeRSA();
key.importKey(configs.apiPrivKey, 'pkcs1-private-pem');

/* Middleware */

var ddos = new Ddos({burst:200, limit:300})
app.use(ddos.express);

app.use(express.text())
app.use(helmet())

app.use((req, res, next)=>{
  if(req.method == "POST"){
    try{
      req.body = JSON.parse(key.decrypt(Buffer.from(req.body, "hex"), 'utf8'))
    }catch{
      req.body = "BAD_CALL"
    }
  }

  next();
})

app.use((req, res, next)=>{
  if(req.body != "BAD_CALL"){
    if(req.body.apiSecret != apiSecret){
      req.body = "BAD_CALL"
    }
  }

  next()
})

/* Server Configs and Routes */

app.post('/api/register', (req, res) => {
  if(req.body != "BAD_CALL"){
    registration(req.body, configs, function(response){
      if(response == "OK"){
        res.end("OK")
      }else if(response == "BAD_USERNAME"){
        res.end("BAD_USERNAME")
      }else if(response == "BAD_EMAIL"){
        res.end("BAD_EMAIL")
      }else{
        res.end("Server side error")
      }
    });
  }else{
    res.end("BAD_CALL")
  }
})

app.post('/api/verify', (req, res) => {
  if(req.body != "BAD_CALL"){
    verification(req.body, function(response){
      if(response == "OK"){
        res.end("OK");
      }else if(response == "WRONG"){
        res.end("WRONG");
      }else if(response == "EXPIRE"){
        res.end("EXPIRE")
      }else{
        res.end("Server side error");
      }
    })
  }else{
    res.end("BAD_CALL")
  }
})

app.post('/api/login', (req, res) => {
  if(req.body != "BAD_CALL"){
    login(req.body, configs, function(response){
      if(response == "INVALID"){
        res.end("INVALID")
      }else if(response == "ERROR"){
        res.end("ERROR")
      }else if(response == "EXPIRE"){
        res.end("EXPIRE");
      }else{
        res.end(response);
      }
    })
  }else{
    res.end("BAD_CALL")
  }
})

app.post('/api/getToken', (req, res) => {
  if(req.body != "BAD_CALL"){
    getToken(req.body, configs, function(response){
      if(response == "INVALID"){
        res.end("INVALID")
      }else{
        res.end(JSON.stringify(response))
      }
    })
  }else{
    res.end("BAD_CALL")
  }
})

app.post('/api/logout', (req, res) => {
  if(req.body != "BAD_CALL"){
    logout(req.body, configs, function(response){
      if(response == "GOOD"){
        res.end("GOOD")
      }else{
        res.end("INVALID")
      }
    })
  }else{
    res.end("BAD_CALL")
  }
})

app.post('/api/getResetCode', (req, res) => {
  if(req.body != "BAD_CALL"){
    getResetCode(req.body, configs, (response) => {
      if(response == "GOOD"){
        res.end("GOOD")
      }else if(response == "ERROR"){
        res.end("ERROR")
      }else if(response == "NO_EXIST"){
        res.end("NO_EXIST")
      }
    })
  }else{
    res.end("BAD_CALL")
  }
})

app.post('/api/resetPassword', (req, res) => {
  if(req.body != "BAD_CALL"){
    resetPassword(req.body, (response) => {
      if(response == "GOOD"){
        res.end("GOOD")
      }else if(response == "INVALID"){
        res.end("INVALID")
      }else if(response == "ERROR"){
        res.end("ERROR")
      }
    })
  }else{
    res.end("BAD_CALL")
  }
})

app.post('/api/changePassword', (req, res) => {
  if(req.body != "BAD_CALL"){
    changePassword(req.body, configs, (response) => {
      if(response == "GOOD"){
        res.end("GOOD")
      }else if(response == "INVALID"){
        res.end("INVALID")
      }
    })
  }else{
    res.end("BAD_CALL")
  }
})

app.post('/api/changeEmail', (req, res) => {
  if(req.body != "BAD_CALL"){
    changeEmail(req.body, configs, (response) => {
      if(response == "GOOD"){
        res.end("GOOD")
      }else if(response == "INVALID"){
        res.end("INVALID")
      }
    })
  }else{
    res.end("BAD_CALL")
  }
})

app.post('/api/deleteAccount', (req, res) => {
  if(req.body != "BAD_CALL"){
    deleteAccount(req.body, configs, (response) => {
      if(response == "GOOD"){
        res.end("GOOD")
      }else if(response == "INVALID"){
        res.end("INVALID")
      }
    })
  }else{
    res.end("BAD_CALL")
  }
})

app.use(function(req, res, next) {
  return res.status(404).send("NO ACCESS");
});

/* Socket Handling */

socketHandling(io, configs);

/* Start Server */

server.listen(3000, function () {
  console.log('Server started, listening on port 3000')
})
